package v.nevolin;

import javafx.application.Application;
import javafx.stage.Stage;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

public class AppMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Model.start();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
