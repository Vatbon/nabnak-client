package v.nevolin.model;

import v.nevolin.entity.api.DashboardDto;
import v.nevolin.entity.api.FTSDto;
import v.nevolin.entity.api.TaskDto;
import v.nevolin.entity.api.UserDto;
import v.nevolin.entity.client.DashboardEntity;
import v.nevolin.entity.db.*;
import v.nevolin.model.net.NetController;
import v.nevolin.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class Model {

    private static final WindowManager windowMananger = new WindowManager();
    private static final NetController netController = new NetController();
    private static final List<State> stateList = new ArrayList<>();
    private static final List<Role> roleList = new ArrayList<>();
    private static User activeUser;
    private static Dashboard activeDashboard;
    private static boolean isConnected = false;
    private static boolean isStart = true;
    private static Settings settings;

    public static Role getRoleById(Long id) {
        for (Role role : getRoleList()) {
            if (role.getId().equals(id)) {
                return role;
            }
        }
        return null;
    }

    public static List<Role> getRoleList() {
        initRoles();
        return roleList;
    }

    public static List<State> getStateList() {
        return stateList;
    }

    public static User getActiveUser() {
        return activeUser;
    }

    public static void setActiveUser(User activeUser) {
        Model.activeUser = activeUser;
    }

    public static void start() {
        settings = new Settings(true);
        isConnected = checkConnection();
        windowMananger.start();
        isStart = false;
    }

    public static boolean checkConnection() {
        if (isStart) {
            return netController.checkConnection(1);
        } else {
            return netController.checkConnection(10);
        }
    }

    private static void initRoles() {
        if (roleList.isEmpty()) {
            roleList.addAll(netController.getRoles());
        }
    }

    public static List<DashboardEntity> getActiveUserDashboards() {
        // here active user
        List<DashboardRole> usersDashboard = netController.getUsersDashboard(activeUser);
        List<DashboardEntity> dashboardEntityList = new ArrayList<>();
        for (DashboardRole role : usersDashboard) {
            DashboardEntity dashboardEntity = new DashboardEntity();
            dashboardEntity.setDashboard(netController.getDashboard(role.getDashboardId()));
            dashboardEntity.setRole(getRoleById(role.getRoleId()));
            dashboardEntityList.add(dashboardEntity);
        }
        return dashboardEntityList;
    }

    public static int login(String login, String password) {
        User user = new User(0L, login, "", password);
        User loggedUser = netController.login(user);
        if (loggedUser == null) {
            return -1;
        }
        activeUser = loggedUser;
        return 0;
    }

    public static int register(User user) {
        User newUser = netController.register(user);
        if (newUser == null) {
            return -1;
        }
        return 0;
    }

    public static int joinDashboard(String hash) {
        int res = netController.joinDashboard(activeUser, hash);
        if (res != 0) {
            return -1;
        }
        return 0;
    }

    public static DashboardDto getDashboardDto(String hash) {
        return netController.getDashboardDto(hash);
    }

    public static User getUserById(Long userId) {
        return netController.getUserById(userId);
    }

    public static void makeTask(Task task, List<Tag> tagList) {
        task.setDashboardId(activeDashboard.getId());
        task.setStateId(0L);
        task.setCreatedByUserId(activeUser.getId());
        TaskDto taskDto = netController.createTask(task);
        for (Tag tag : tagList) {
            netController.addTag(new Tag(taskDto.getTask().getId(), tag.getName()));
        }
    }

    public static TaskDto editTask(Task task, List<Tag> tagList) {
        TaskDto taskDto = new TaskDto();
        taskDto.setTask(task);
        taskDto.setTagList(tagList);
        return netController.update(taskDto);
    }

    public static void setActiveDashboard(Dashboard activeDashboard) {
        Model.activeDashboard = activeDashboard;
    }

    public static Dashboard getActiveDashboard() {
        return activeDashboard;
    }

    public static void changeState(Long activeTaskId, Long stateId) {
        netController.changeState(activeTaskId, stateId);
    }

    public static void createDashboard(String dashboardName) {
        Dashboard dashboard = new Dashboard();
        dashboard.setName(dashboardName);
        netController.createDashboard(dashboard, activeUser.getId());
        WindowManager.updateDashboardListScene();
    }

    public static Settings getSettings() {
        return settings;
    }

    public static void setSettings(Settings newSettings) {
        settings = newSettings;
    }

    public static void leaveDashboard(Dashboard dashboard) {
        netController.leaveDashboard(dashboard.getHash(), activeUser);
    }

    public static Comment createComment(Comment comment) {
        return netController.makeComment(comment);
    }

    public static Log createLog(Log log) {
        return netController.makeLog(log);
    }

    public static List<UserDto> getUsersByHash(String hash) {
        return netController.getUsersByHash(hash);
    }

    public static void changeRole(UserDto userDto) {
        netController.changeRole(userDto, activeUser.getId(), getActiveDashboard().getHash());
        WindowManager.updateDashboard();
    }

    public static void showError(String error) {
        WindowManager.showError(error);
    }

    public static List<Task> getFtsTasks(Long dashboardId, String text) {
        FTSDto ftsDto = new FTSDto();
        ftsDto.setText(text);
        ftsDto.setUserId(getActiveUser().getId());
        return netController.ftsTasks(ftsDto, dashboardId);
    }

    public static List<Log> getFtsLogs(Long dashboardId, String text) {
        FTSDto ftsDto = new FTSDto();
        ftsDto.setText(text);
        ftsDto.setUserId(getActiveUser().getId());
        return netController.ftsLogs(ftsDto, dashboardId);
    }

    public static List<Comment> getFtsComments(Long dashboardId, String text) {
        FTSDto ftsDto = new FTSDto();
        ftsDto.setText(text);
        ftsDto.setUserId(getActiveUser().getId());
        return netController.ftsComments(ftsDto, dashboardId);
    }
}
