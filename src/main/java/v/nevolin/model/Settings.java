package v.nevolin.model;

import java.io.InputStream;
import java.util.Properties;

public class Settings {
    private String host = "http://localhost:8080";


    public Settings(boolean init) {
        if (init) {
            String propFileName = "settings.properties";
            try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);) {
                Properties prop = new Properties();
                if (inputStream != null) {
                    prop.load(inputStream);
                }
                host = prop.getProperty("defaultHost");
            } catch (Exception ignored) {
            }
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
