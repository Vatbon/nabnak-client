package v.nevolin.model.net;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import v.nevolin.entity.api.DashboardDto;
import v.nevolin.entity.api.FTSDto;
import v.nevolin.entity.api.TaskDto;
import v.nevolin.entity.api.UserDto;
import v.nevolin.entity.db.*;
import v.nevolin.model.Model;

import java.net.ConnectException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpTimeoutException;
import java.time.Duration;
import java.util.List;

public class NetController {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String ACCEPT = "Accept";
    private static final String APPLICATION_JSON = "application/json";
    private static final Duration TIMEOUT_DURATION = Duration.ofSeconds(10);
    private static final String CONTENT_TYPE = "Content-Type";

    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    private void checkError(HttpResponse<String> send) {
        if (send.statusCode() == 403) {
            Model.showError(send.body());
        }
    }

    @SneakyThrows
    public Dashboard getDashboard(Long id) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(getHost() + "/api/v1/dashboard/entity?id=" + id))
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        return mapper.readValue(send.body(), Dashboard.class);
    }

    @SneakyThrows
    public List<DashboardRole> getUsersDashboard(User user) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(getHost() + "/api/v1/dashboards?userId=" + user.getId()))
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        return mapper.readValue(send.body(),
                mapper.getTypeFactory().constructCollectionType(List.class, DashboardRole.class));
    }

    @SneakyThrows
    public DashboardDto getDashboardDto(String hash) {
        HttpRequest request1 = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(getHost() + "/api/v1/dashboard?hash=" + hash))
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> dashboard = httpClient.send(request1, HttpResponse.BodyHandlers.ofString());
        checkError(dashboard);
        return mapper.readValue(dashboard.body(), DashboardDto.class);
    }

    @SneakyThrows
    public List<Role> getRoles() {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(getHost() + "/api/v1/roles"))
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        return mapper.readValue(send.body(),
                mapper.getTypeFactory().constructCollectionType(List.class, Role.class));
    }

    @SneakyThrows
    public User login(User user) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(user)))
                .uri(URI.create(getHost() + "/api/v1/login"))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return null;
        }
        return mapper.readValue(send.body(), User.class);

    }

    @SneakyThrows
    public User register(User user) {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(user)))
                .uri(URI.create(getHost() + "/api/v1/register"))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return null;
        }
        return mapper.readValue(send.body(), User.class);
    }

    @SneakyThrows
    public int joinDashboard(User user, String hash) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(user)))
                .uri(URI.create(getHost() + "/api/v1/dashboard/join?hash=" + hash))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200) {
            return -1;
        }
        return 0;
    }

    @SneakyThrows
    public User getUserById(Long userId) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(getHost() + "/api/v1/user?id=" + userId))
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return null;
        }
        return mapper.readValue(send.body(), User.class);
    }

    @SneakyThrows
    public TaskDto createTask(Task task) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(task)))
                .uri(URI.create(getHost() + "/api/v1/task"))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return null;
        }
        return mapper.readValue(send.body(), TaskDto.class);
    }

    @SneakyThrows
    public int addTag(Tag tag) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(tag)))
                .uri(URI.create(getHost() + "/api/v1/tag/add"))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200) {
            return -1;
        }
        return 0;
    }

    @SneakyThrows
    public void changeState(Long activeTaskId, Long stateId) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.noBody())
                .uri(URI.create(getHost() + "/api/v1/task/changestate?taskId=" + activeTaskId + "&stateId=" + stateId))
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
    }

    @SneakyThrows
    public TaskDto update(TaskDto taskDto) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(taskDto)))
                .uri(URI.create(getHost() + "/api/v1/task/update"))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return null;
        }
        return mapper.readValue(send.body(), TaskDto.class);
    }

    @SneakyThrows
    public Dashboard createDashboard(Dashboard dashboard, Long userId) {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(dashboard)))
                .uri(URI.create(getHost() + "/api/v1/dashboard/create?userId=" + userId))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return null;
        }
        return mapper.readValue(send.body(), Dashboard.class);
    }

    @SneakyThrows
    public boolean checkConnection(int seconds) {
        try {
            Duration duration = TIMEOUT_DURATION;
            if (seconds > 0 && seconds < TIMEOUT_DURATION.getSeconds()) {
                duration = Duration.ofSeconds(seconds);
            }
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(getHost()))
                    .header(ACCEPT, APPLICATION_JSON)
                    .timeout(duration)
                    .build();
            HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            checkError(send);
            if (send.statusCode() != 200) {
                return false;
            }
        } catch (ConnectException | HttpTimeoutException e) {
            return false;
        }
        return true;
    }

    @SneakyThrows
    public int leaveDashboard(String hash, User user) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(user)))
                .uri(URI.create(getHost() + "/api/v1/dashboard/leave?hash=" + hash))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200) {
            return -1;
        }
        return 0;
    }

    @SneakyThrows
    public Comment makeComment(Comment comment) {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(comment)))
                .uri(URI.create(getHost() + "/api/v1/comment/create"))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200) {
            return null;
        }
        return mapper.readValue(send.body(), Comment.class);
    }

    @SneakyThrows
    public Log makeLog(Log log) {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(log)))
                .uri(URI.create(getHost() + "/api/v1/log/create"))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200) {
            return null;
        }
        return mapper.readValue(send.body(), Log.class);
    }

    @SneakyThrows
    public List<UserDto> getUsersByHash(String hash) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(getHost() + "/api/v1/user/dashboard?hash=" + hash))
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return List.of();
        }
        return mapper.readValue(send.body(),
                mapper.getTypeFactory().constructCollectionType(List.class, UserDto.class));
    }

    @SneakyThrows
    public int changeRole(UserDto userDto, Long adminId, String hash) {
        String params = "?hash=" + hash
                + "&adminId=" + adminId
                + "&userId=" + userDto.getUser().getId()
                + "&roleId=" + userDto.getRoleId();
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.noBody())
                .uri(URI.create(getHost() + "/api/v1/dashboard/changerole" + params))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200) {
            return -1;
        }
        return 0;
    }

    private String getHost() {
        String host = Model.getSettings().getHost();
        if (host.startsWith("http://")) {
            return host;
        } else if (host.startsWith("https://")) {
            return "http://" + host.substring(8);
        } else {
            return "http://" + host;
        }
    }

    @SneakyThrows
    public List<Task> ftsTasks(FTSDto ftsDto, Long dashboardId) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(ftsDto)))
                .uri(URI.create(getHost() + "/api/v1/fts/tasks?dashboardId=" + dashboardId))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return List.of();
        }
        return mapper.readValue(send.body(),
                mapper.getTypeFactory().constructCollectionType(List.class, Task.class));
    }

    @SneakyThrows
    public List<Log> ftsLogs(FTSDto ftsDto, Long dashboardId) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(ftsDto)))
                .uri(URI.create(getHost() + "/api/v1/fts/logs?dashboardId=" + dashboardId))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return List.of();
        }
        return mapper.readValue(send.body(),
                mapper.getTypeFactory().constructCollectionType(List.class, Log.class));
    }

    @SneakyThrows
    public List<Comment> ftsComments(FTSDto ftsDto, Long dashboardId) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(ftsDto)))
                .uri(URI.create(getHost() + "/api/v1/fts/comments?dashboardId=" + dashboardId))
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .timeout(TIMEOUT_DURATION)
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        checkError(send);
        if (send.statusCode() != 200 || send.body().isEmpty()) {
            return List.of();
        }
        return mapper.readValue(send.body(),
                mapper.getTypeFactory().constructCollectionType(List.class, Comment.class));
    }
}
