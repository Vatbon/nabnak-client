package v.nevolin.entity.client;

import lombok.Data;
import v.nevolin.entity.db.Dashboard;
import v.nevolin.entity.db.Role;

@Data
public class DashboardEntity {
    private Dashboard dashboard;
    private Role role;
}
