package v.nevolin.entity.api;

import lombok.Data;
import v.nevolin.entity.db.Comment;
import v.nevolin.entity.db.Dashboard;
import v.nevolin.entity.db.DashboardRole;
import v.nevolin.entity.db.Log;

import java.util.List;

@Data
public class DashboardDto {
    private Dashboard dashboard;
    private List<DashboardRole> dashboardRoleList;
    private List<TaskDto> taskList;
    private List<Comment> commentList;
    private List<Log> logList;
}
