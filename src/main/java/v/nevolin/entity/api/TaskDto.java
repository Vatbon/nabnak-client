package v.nevolin.entity.api;

import lombok.Data;
import v.nevolin.entity.db.Tag;
import v.nevolin.entity.db.Task;

import java.util.List;

@Data
public class TaskDto {
    private Task task;
    private List<Tag> tagList;
}
