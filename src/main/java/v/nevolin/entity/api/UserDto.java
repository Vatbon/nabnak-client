package v.nevolin.entity.api;

import lombok.Data;
import v.nevolin.entity.db.User;

@Data
public class UserDto {
    private User user;
    private Long roleId;
}
