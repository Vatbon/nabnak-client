package v.nevolin.entity.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    private Long id;
    private Long authorUserId;
    private Long taskId;
    private String body;
    private String createdTime;
}
