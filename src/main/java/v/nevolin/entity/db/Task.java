package v.nevolin.entity.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    private Long id;
    private Long createdByUserId;
    private Long assignedToUserId;
    private Long dashboardId;
    private String name;
    private String description;
    private String createdTime;
    private String lastUpdatedTime;
    private Long parentTaskId;
    private Long stateId;
}
