package v.nevolin.entity.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashboardRole {
    private Long dashboardId;
    private Long userId;
    private Long roleId;
}
