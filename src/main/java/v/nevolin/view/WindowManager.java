package v.nevolin.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import v.nevolin.entity.api.TaskDto;
import v.nevolin.entity.api.UserDto;
import v.nevolin.entity.client.DashboardEntity;
import v.nevolin.entity.db.Comment;
import v.nevolin.entity.db.Log;
import v.nevolin.model.Model;
import v.nevolin.view.controller.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WindowManager {
    private static Stage activeStage;
    private static Stage taskStage;
    private static Stage settingStage;
    private static DashboardController activeDashboardController;
    private static DashboardsController activeDashboardsController;
    private static Image icon = new Image(WindowManager.class.getResourceAsStream("/icon.png"));

    public static DashboardController getActiveDashboardController() {
        return activeDashboardController;
    }

    @SneakyThrows
    private static Parent getParent(String fxmlPath) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource(fxmlPath);
        loader.setLocation(xmlUrl);
        return loader.load();
    }

    @SneakyThrows
    public static void openDashboards() {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/dashboards.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        DashboardsController dashboardsController = loader.getController();
        dashboardsController.init();
        activeDashboardsController = dashboardsController;
        setScene(new Scene(root), true);
    }

    @SneakyThrows
    public static void openLogin() {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/login.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        LoginController loginController = loader.getController();
        loginController.setConnected(Model.checkConnection());
        setScene(new Scene(root), true);
    }

    @SneakyThrows
    public static void openRegister() {
        Parent root = getParent("/register.fxml");
        setScene(new Scene(root), true);
    }

    @SneakyThrows
    public static void openDashboard(String hash) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/dashboard.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        DashboardController dashboardController = loader.getController();
        dashboardController.setDashboardDto(Model.getDashboardDto(hash));
        dashboardController.init();
        activeDashboardController = dashboardController;
        setScene(new Scene(root), true);
    }

    @SneakyThrows
    public static void openEdit(TaskDto taskDto) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/task.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        TaskController taskController = loader.getController();
        taskController.setTask(taskDto);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        taskStage = stage;
        stage.show();
    }

    @SneakyThrows
    public static void openCreate() {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/task.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        TaskController taskController = loader.getController();
        taskController.setEmpty();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        taskStage = stage;
        stage.show();
    }

    private static void setScene(Scene scene, boolean toShow) {
        if (activeStage == null) {
            Stage stage = new Stage();
            stage.setTitle("Nabnak");
            stage.setScene(scene);
            setActiveStage(stage, toShow);
        }
        activeStage.setScene(scene);
        activeStage.show();
    }

    private static void setActiveStage(Stage stage, boolean toShow) {
        if (activeStage != null) {
            activeStage.close();
        }
        stage.getIcons().add(icon);
        activeStage = stage;
        if (toShow) {
            activeStage.show();
        }
    }

    public static List<DashboardEntity> getDashboards() {
        return Model.getActiveUserDashboards();
    }

    public static void closeTaskWindow() {
        if (taskStage != null) {
            taskStage.close();
        }
    }

    public static void updateDashboard() {
        activeDashboardController.setDashboardDto(Model.getDashboardDto(Model.getActiveDashboard().getHash()));
    }

    public static void updateDashboardListScene() {
        activeDashboardsController.init();
    }

    @SneakyThrows
    public static void openSettings() {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/settings.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        SettingsController settingsController = loader.getController();
        settingsController.setParameters(Model.getSettings());
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        settingStage = stage;
        stage.show();
    }

    public static void closeSettings() {
        settingStage.close();
    }

    @SneakyThrows
    public static void openComments(Long taskId) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/comments.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        CommentsController commentsController = loader.getController();
        List<Comment> comments = new ArrayList<>();
        for (Comment comment : Model.getDashboardDto(Model.getActiveDashboard().getHash()).getCommentList()) {
            if (comment.getTaskId().equals(taskId)) {
                comments.add(comment);
            }
        }
        commentsController.setComments(comments);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        stage.show();
    }

    @SneakyThrows
    public static void openLogs(Long activeTaskId) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/logs.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        LogsController logsController = loader.getController();
        List<Log> logs = new ArrayList<>();
        for (Log log : Model.getDashboardDto(Model.getActiveDashboard().getHash()).getLogList()) {
            if (log.getTaskId().equals(activeTaskId)) {
                logs.add(log);
            }
        }
        logsController.setLogs(logs);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        stage.show();
    }

    @SneakyThrows
    public static void openUserEdit(UserDto userDto) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/userEdit.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        UserEditController userEditController = loader.getController();
        userEditController.setUser(userDto);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        stage.show();
    }

    @SneakyThrows
    public static void showError(String error) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/error.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        ErrorController userEditController = loader.getController();
        userEditController.setErrorMessage(error);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        stage.setTitle("Error");
        userEditController.setStage(stage);
        stage.show();
    }

    @SneakyThrows
    public static void openFts() {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = WindowManager.class.getResource("/fts.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        FTSController ftsController = loader.getController();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        stage.setTitle("Search");
        ftsController.setDashboardList(getDashboards());
        ftsController.init();
        stage.show();
    }

    public static void openTaskFromFts(Long dashboardId, Long taskId) {
        DashboardEntity dashboardEntity = null;
        for (DashboardEntity userDashboard : Model.getActiveUserDashboards()) {
            if (userDashboard.getDashboard().getId().equals(dashboardId)) {
                dashboardEntity = userDashboard;
                break;
            }
        }
        if (dashboardEntity != null) {
            WindowManager.openDashboard(dashboardEntity.getDashboard().getHash());
            activeDashboardController.setActiveTaskId(taskId);
            activeDashboardController.showTaskInfo(taskId);
        }
    }

    public static void openLogFromFts(Long dashboardId, Long taskId) {
        DashboardEntity dashboardEntity = null;
        for (DashboardEntity userDashboard : Model.getActiveUserDashboards()) {
            if (userDashboard.getDashboard().getId().equals(dashboardId)) {
                dashboardEntity = userDashboard;
                break;
            }
        }
        if (dashboardEntity != null) {
            WindowManager.openDashboard(dashboardEntity.getDashboard().getHash());
            activeDashboardController.setActiveTaskId(taskId);
            activeDashboardController.showTaskInfo(taskId);
            activeDashboardController.openLogs();
        }
    }

    public static void openCommentsFromFts(Long dashboardId, Long taskId) {
        DashboardEntity dashboardEntity = null;
        for (DashboardEntity userDashboard : Model.getActiveUserDashboards()) {
            if (userDashboard.getDashboard().getId().equals(dashboardId)) {
                dashboardEntity = userDashboard;
                break;
            }
        }
        if (dashboardEntity != null) {
            WindowManager.openDashboard(dashboardEntity.getDashboard().getHash());
            activeDashboardController.setActiveTaskId(taskId);
            activeDashboardController.showTaskInfo(taskId);
            activeDashboardController.openComments();
        }
    }

    @SneakyThrows
    public void start() {
        openLogin();
    }
}
