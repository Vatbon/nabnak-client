package v.nevolin.view.controller;

import javafx.util.Pair;
import v.nevolin.entity.api.TaskDto;
import v.nevolin.entity.db.Tag;

public class StyleGenerator {

    private StyleGenerator() {
    }

    public static Pair<String, String> getRandomTaskStyle(TaskDto taskDto) {
        String darkStyle;
        String brightStyle;
        Pair<String, String> randomPair;
        if (taskDto == null) {
            randomPair = ColorGenerator.getRandomPair();
        } else {
            randomPair = ColorGenerator.getRandomPair(taskDto.getTask().getId().intValue());
        }
        darkStyle = "-fx-border-width: 5;" +
                " -fx-border-radius: 20;" +
                " -fx-background-radius: 25;" +
                " -fx-background-color: " + randomPair.getValue() + ";" +
                " -fx-border-color: " + randomPair.getKey() + ";";
        brightStyle = "-fx-border-width: 5;" +
                " -fx-border-radius: 20;" +
                " -fx-background-radius: 25;" +
                " -fx-background-color: " + randomPair.getValue() + ";" +
                " -fx-border-color: " + randomPair.getValue() + ";";
        return new Pair<>(darkStyle, brightStyle);
    }

    public static String getRandomTagStyle(Tag tag) {
        String randomColor;
        if (tag != null) {
            int t = 0;
            for (byte b : tag.getName().getBytes()) {
                t += Math.abs(b);
            }
            randomColor = ColorGenerator.getRandomColor(t);
        } else {
            randomColor = ColorGenerator.getRandomColor();
        }
        return "-fx-background-color: " + randomColor + ";" +
                " -fx-background-radius: 10;";
    }
}
