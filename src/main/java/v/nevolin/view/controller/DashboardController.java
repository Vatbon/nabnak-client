package v.nevolin.view.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.util.Pair;
import v.nevolin.entity.api.DashboardDto;
import v.nevolin.entity.api.TaskDto;
import v.nevolin.entity.api.UserDto;
import v.nevolin.entity.db.Comment;
import v.nevolin.entity.db.Log;
import v.nevolin.entity.db.Tag;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class DashboardController {

    @FXML
    public FlowPane inProgressTilePane;
    @FXML
    public FlowPane toDoTilePane;
    @FXML
    public FlowPane doneTilePane;
    @FXML
    public TilePane usersPane;
    /*object associated with info on the right side*/
    @FXML
    public Label taskNameLabel;
    @FXML
    public Label taskStateLabel;
    @FXML
    public Label assignedToLabel;
    @FXML
    public Label createdByLabel;
    @FXML
    public Label createdAtLabel;
    @FXML
    public Label lastUpdatedLabel;
    @FXML
    public Label descriptionLabel;
    @FXML
    public Button editButton;
    @FXML
    public Button commentButton;
    @FXML
    public Button logButton;
    @FXML
    public Button newTaskButton;
    @FXML
    public Button moveButton;
    @FXML
    public TextField dashboardHashField;
    @FXML
    public Label dashboardName;
    @FXML
    public Button leaveButton;
    @FXML
    public Button backButton;
    @FXML
    public Button updateButton;
    @FXML
    public FlowPane flowPaneDescription;
    @FXML
    public Label descriptionLiterallyLabel;

    private DashboardDto dashboardDto;
    private Long activeTaskId;

    public Long getActiveTaskId() {
        return activeTaskId;
    }

    public void init() {
        taskNameLabel.setVisible(false);
        taskStateLabel.setVisible(false);
        assignedToLabel.setVisible(false);
        createdByLabel.setVisible(false);
        createdAtLabel.setVisible(false);
        lastUpdatedLabel.setVisible(false);
        descriptionLabel.setVisible(false);
        descriptionLiterallyLabel.setVisible(false);
        flowPaneDescription.setVisible(false);
        editButton.setVisible(false);
        commentButton.setVisible(false);
        editButton.setVisible(false);
        logButton.setVisible(false);
        moveButton.setVisible(false);
        dashboardName.setText(dashboardDto.getDashboard().getName());
        dashboardHashField.setText(dashboardDto.getDashboard().getHash());
    }

    public void setDashboardDto(DashboardDto dashboardDto) {
        this.dashboardDto = dashboardDto;
        Model.setActiveDashboard(dashboardDto.getDashboard());
        toDoTilePane.getChildren().clear();
        inProgressTilePane.getChildren().clear();
        doneTilePane.getChildren().clear();
        for (TaskDto taskDto : dashboardDto.getTaskList()) {
            GridPane gridPane = makeGridPane(taskDto);
            if (taskDto.getTask().getStateId().equals(0L)) {
                toDoTilePane.getChildren().add(gridPane);
            } else if (taskDto.getTask().getStateId().equals(1L)) {
                inProgressTilePane.getChildren().add(gridPane);
            } else if (taskDto.getTask().getStateId().equals(2L)) {
                doneTilePane.getChildren().add(gridPane);
            }
        }
        setUserPane();
    }

    private void setUserPane() {
        List<UserDto> userList = Model.getUsersByHash(dashboardDto.getDashboard().getHash());
        List<UserDto> admins = new ArrayList<>();
        List<UserDto> participants = new ArrayList<>();
        List<UserDto> guests = new ArrayList<>();
        ObservableList<Node> paneChildren = usersPane.getChildren();

        for (UserDto userDto : userList) {
            if (userDto.getRoleId() == 0L) {
                admins.add(userDto);
            } else if (userDto.getRoleId() == 1L) {
                participants.add(userDto);
            } else if (userDto.getRoleId() == 2L) {
                guests.add(userDto);
            }
        }
        paneChildren.clear();
        Label adminLabel = new Label("Admins");
        adminLabel.setPrefWidth(194.0);
        paneChildren.add(adminLabel);
        for (UserDto admin : admins) {
            Label guestL = makeUserLabel(admin);
            paneChildren.add(guestL);
        }

        Label participantLabel = new Label("Participants");
        participantLabel.setPrefWidth(194.0);
        paneChildren.add(participantLabel);
        for (UserDto participant : participants) {
            Label guestL = makeUserLabel(participant);
            paneChildren.add(guestL);
        }

        Label guestLabel = new Label("Guests");
        guestLabel.setPrefWidth(194.0);
        paneChildren.add(guestLabel);
        for (UserDto guest : guests) {
            Label guestL = makeUserLabel(guest);
            paneChildren.add(guestL);
        }
    }

    private Label makeUserLabel(UserDto userDto) {
        Label userLabel = new Label();
        userLabel.setText(userDto.getUser().getName() + " | " + userDto.getUser().getEmail());
        userLabel.setOnMouseEntered(event -> userLabel.setUnderline(true));
        userLabel.setOnMouseExited(event -> userLabel.setUnderline(false));
        userLabel.setOnMouseClicked(event -> WindowManager.openUserEdit(userDto));
        userLabel.setPrefWidth(194.0);
        return userLabel;
    }

    private GridPane makeGridPane(TaskDto taskDto) {
        GridPane gridPane = makeTaskPane(taskDto);
        Insets insets = new Insets(2.5, 5, 2.5, 5);
        FlowPane.setMargin(gridPane, insets);

        GridPane bottomPane = makeBottomPane(taskDto);
        gridPane.add(bottomPane, 0, 3);

        FlowPane flowPaneTags = new FlowPane();
        for (Tag tag : taskDto.getTagList()) {
            Label tagLabel = makeLabelTag(tag);
            flowPaneTags.getChildren().add(tagLabel);
        }

        GridPane.setMargin(flowPaneTags, new Insets(7.5, 7.5, 0, 7.5));
        if (!flowPaneTags.getChildren().isEmpty()) {
            gridPane.add(flowPaneTags, 0, 0);
        }

        Label nameLabel = new Label();
        nameLabel.setText(taskDto.getTask().getName());
        nameLabel.setFont(new Font(22));
        nameLabel.setPadding(new Insets(7.5));
        gridPane.add(nameLabel, 0, 1);

        Label subJobLabel = new Label();
        TaskDto taskDtoParent = findTaskById(taskDto.getTask().getParentTaskId());
        if (taskDtoParent != null) {
            String name = taskDtoParent.getTask().getName();
            if (!name.isEmpty()) {
                subJobLabel.setText("subjob of " + name);
                gridPane.add(subJobLabel, 0, 2);
            }
        }

        return gridPane;
    }

    private GridPane makeBottomPane(TaskDto taskDto) {
        GridPane bottomPane = new GridPane();
        RowConstraints rowConstraints = new RowConstraints();
        bottomPane.getRowConstraints().add(rowConstraints);

        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setMinWidth(10);
        columnConstraints.setPrefWidth(100);
        bottomPane.getColumnConstraints().add(columnConstraints);

        ColumnConstraints columnConstraints1 = new ColumnConstraints();
        columnConstraints1.setMinWidth(10);
        columnConstraints1.setPrefWidth(100);
        bottomPane.getColumnConstraints().add(columnConstraints1);

        Label commentLabel = new Label();
        commentLabel.setText("Comments: " + countComments(taskDto.getTask().getId()));
        commentLabel.setPadding(new Insets(5));
        Label timeLabel = new Label();
        timeLabel.setText("Time: " + countTime(taskDto.getTask().getId()));
        timeLabel.setPadding(new Insets(5));
        GridPane.setConstraints(timeLabel, 1, 0);
        GridPane.setConstraints(commentLabel, 0, 0);
        GridPane.setHalignment(commentLabel, HPos.LEFT);
        GridPane.setHalignment(timeLabel, HPos.RIGHT);
        bottomPane.getChildren().addAll(commentLabel, timeLabel);
        return bottomPane;
    }

    private GridPane makeTaskPane(TaskDto taskDto) {
        GridPane gridPane = new GridPane();

        gridPane.setMaxHeight(Integer.MAX_VALUE);
        gridPane.setPrefWidth(302.0);
        gridPane.getColumnConstraints().clear();
        ColumnConstraints col = new ColumnConstraints();
        col.setHgrow(Priority.ALWAYS);
        col.setMinWidth(10);
        col.setPercentWidth(100);
        col.setPrefWidth(100);
        gridPane.getColumnConstraints().add(col);
        gridPane.getRowConstraints().clear();

        for (int i = 0; i < 4; i++) {
            RowConstraints row = new RowConstraints();
            row.setFillHeight(false);
            gridPane.getRowConstraints().add(row);
        }

        Pair<String, String> styles = StyleGenerator.getRandomTaskStyle(taskDto);
        String darkStyle = styles.getKey();
        String brightStyle = styles.getValue();
        gridPane.setStyle(darkStyle);
        gridPane.setOnMouseEntered(event -> gridPane.setStyle(brightStyle));
        gridPane.setOnMouseExited(event -> gridPane.setStyle(darkStyle));

        gridPane.setOnMouseClicked(new TaskEventHandler(taskDto.getTask().getId(), this));
        return gridPane;
    }

    private Label makeLabelTag(Tag tag) {
        Label tagLabel = new Label();
        tagLabel.setFont(new Font(11));
        tagLabel.setText(tag.getName());
        tagLabel.setPadding(new Insets(1, 3, 1, 3));
        tagLabel.setStyle(StyleGenerator.getRandomTagStyle(tag));
        FlowPane.setMargin(tagLabel, new Insets(1, 2, 1, 2));
        return tagLabel;
    }

    private int countTime(Long id) {
        int res = 0;
        for (Log log : dashboardDto.getLogList()) {
            if (log.getTaskId().equals(id))
                res += log.getTime();
        }
        return res;
    }

    private int countComments(Long id) {
        int res = 0;
        for (Comment comment : dashboardDto.getCommentList()) {
            if (comment.getTaskId().equals(id))
                res++;
        }
        return res;
    }

    private TaskDto findTaskById(Long taskId) {
        for (TaskDto taskDto : dashboardDto.getTaskList()) {
            if (taskDto.getTask().getId().equals(taskId)) {
                return taskDto;
            }
        }
        return null;
    }

    public void showTaskInfo(Long id) {
        taskNameLabel.setVisible(true);
        taskStateLabel.setVisible(true);
        assignedToLabel.setVisible(true);
        createdByLabel.setVisible(true);
        createdAtLabel.setVisible(true);
        lastUpdatedLabel.setVisible(true);
        descriptionLabel.setVisible(true);
        descriptionLiterallyLabel.setVisible(true);
        flowPaneDescription.setVisible(true);
        editButton.setVisible(true);
        commentButton.setVisible(true);
        editButton.setVisible(true);
        logButton.setVisible(true);
        moveButton.setVisible(true);

        TaskDto taskDto = findTaskById(id);
        if (taskDto != null) {
            taskNameLabel.setText(taskDto.getTask().getName());
            if (taskDto.getTask().getAssignedToUserId() != null) {
                assignedToLabel.setText("Assigned to: " + Model.getUserById(taskDto.getTask().getAssignedToUserId()).getName());
            } else {
                assignedToLabel.setText("Assigned to: ");

            }
            createdByLabel.setText("Created by: " + Model.getUserById(taskDto.getTask().getCreatedByUserId()).getName());
            createdAtLabel.setText("Created at: " + taskDto.getTask().getCreatedTime());
            lastUpdatedLabel.setText("Updated at: " + taskDto.getTask().getLastUpdatedTime());
            descriptionLabel.setText(taskDto.getTask().getDescription());
            if (taskDto.getTask().getStateId().equals(0L)) {
                taskStateLabel.setText("To Do");
            } else if (taskDto.getTask().getStateId().equals(1L)) {
                taskStateLabel.setText("In Progress");
            } else if (taskDto.getTask().getStateId().equals(2L)) {
                taskStateLabel.setText("Done");
            }
        }
    }

    public void editClicked() {
        WindowManager.openEdit((TaskDto) dashboardDto.getTaskList().stream().
                filter(taskDto -> taskDto.getTask().getId().equals(activeTaskId)).toArray()[0]);
    }

    public void setActiveTaskId(Long taskId) {
        activeTaskId = taskId;
    }

    public void newClicked() {
        WindowManager.openCreate();
    }

    @FXML
    public void moveTaskClicked() {
        TaskDto taskDto = findTaskById(activeTaskId);
        if (taskDto != null) {
            Model.changeState(activeTaskId, Math.min(3, taskDto.getTask().getStateId() + 1));
            WindowManager.updateDashboard();
        }
    }

    public void backClicked() {
        WindowManager.openDashboards();
    }

    public void leaveDashboard() {
        Model.leaveDashboard(dashboardDto.getDashboard());
        WindowManager.openDashboards();
    }

    public void openComments() {
        WindowManager.openComments(activeTaskId);
    }

    public void openLogs() {
        WindowManager.openLogs(activeTaskId);
    }

    public void update() {
        WindowManager.updateDashboard();
    }
}
