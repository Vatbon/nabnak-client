package v.nevolin.view.controller;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class TaskEventHandler implements EventHandler<MouseEvent> {

    private Long taskId;
    private DashboardController controller;

    public TaskEventHandler(Long taskId, DashboardController controller) {
        this.taskId = taskId;
        this.controller = controller;
    }

    @Override
    public void handle(MouseEvent event) {
        controller.setActiveTaskId(taskId);
        controller.showTaskInfo(taskId);
    }
}
