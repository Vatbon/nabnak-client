package v.nevolin.view.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import v.nevolin.entity.client.DashboardEntity;
import v.nevolin.entity.db.Comment;
import v.nevolin.entity.db.Log;
import v.nevolin.entity.db.Task;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

import java.util.List;

public class FTSController {
    @FXML
    public ChoiceBox<String> dashboardChoice;
    @FXML
    public ChoiceBox<String> typeChoice;
    @FXML
    public TextField textField;
    @FXML
    public TilePane tilePane;

    private enum types {TASKS, COMMENTS, LOGS}

    private List<DashboardEntity> dashboardList;

    public void init() {
        ObservableList<String> items = typeChoice.getItems();
        for (types value : types.values()) {
            items.add(value.toString());
        }
        ObservableList<String> choiceItems = dashboardChoice.getItems();
        for (DashboardEntity dashboardEntity : dashboardList) {
            choiceItems.add(dashboardEntity.getDashboard().getName());
        }
        tilePane.getChildren().clear();
    }

    public void setDashboardList(List<DashboardEntity> dashboards) {
        this.dashboardList = dashboards;
    }

    public void makeSearch() {
        if (typeChoice.getValue() != null && dashboardChoice.getValue() != null) {
            switch (types.valueOf(typeChoice.getValue())) {
                case TASKS:
                    showTasks();
                    break;
                case LOGS:
                    showLogs();
                    break;
                case COMMENTS:
                    showComments();
                    break;
            }
        }
    }

    private void showComments() {
        Long dashboardId = -1L;
        String text;
        for (DashboardEntity dashboardEntity : dashboardList) {
            if (dashboardEntity.getDashboard().getName().equals(dashboardChoice.getValue()))
                dashboardId = dashboardEntity.getDashboard().getId();
        }
        text = textField.getText();
        if (dashboardId != -1L) {
            List<Comment> ftsComments = Model.getFtsComments(dashboardId, text);
            tilePane.getChildren().clear();
            for (Comment ftsComment : ftsComments) {
                Long finalDashboardId = dashboardId;
                GridPane gridPane = makeGridPaneResult(Model.getUserById(ftsComment.getAuthorUserId()).getName(),
                        ftsComment.getBody(),
                        event -> WindowManager.openCommentsFromFts(finalDashboardId, ftsComment.getTaskId()));
                tilePane.getChildren().add(gridPane);
            }
        }
    }

    private void showLogs() {
        Long dashboardId = -1L;
        String text;
        for (DashboardEntity dashboardEntity : dashboardList) {
            if (dashboardEntity.getDashboard().getName().equals(dashboardChoice.getValue()))
                dashboardId = dashboardEntity.getDashboard().getId();
        }
        text = textField.getText();
        if (dashboardId != -1L) {
            List<Log> ftsLogs = Model.getFtsLogs(dashboardId, text);
            tilePane.getChildren().clear();
            for (Log ftsLog : ftsLogs) {
                Long finalDashboardId = dashboardId;
                GridPane gridPane = makeGridPaneResult(Model.getUserById(ftsLog.getAuthorUserId()).getName(),
                        ftsLog.getComment(),
                        event -> WindowManager.openLogFromFts(finalDashboardId, ftsLog.getTaskId()));
                tilePane.getChildren().add(gridPane);
            }
        }
    }

    private void showTasks() {
        Long dashboardId = -1L;
        String text;
        for (DashboardEntity dashboardEntity : dashboardList) {
            if (dashboardEntity.getDashboard().getName().equals(dashboardChoice.getValue()))
                dashboardId = dashboardEntity.getDashboard().getId();
        }
        text = textField.getText();
        if (dashboardId != -1L) {
            List<Task> ftsTasks = Model.getFtsTasks(dashboardId, text);
            tilePane.getChildren().clear();
            for (Task ftsTask : ftsTasks) {
                GridPane gridPane = makeGridPaneResult(ftsTask.getName(),
                        ftsTask.getDescription(),
                        event -> WindowManager.openTaskFromFts(ftsTask.getDashboardId(), ftsTask.getId()));
                tilePane.getChildren().add(gridPane);
            }
        }
    }

    private GridPane makeGridPaneResult(String name, String text, EventHandler<ActionEvent> eventHandler) {
        GridPane gridPane = new GridPane();
        gridPane.setPrefHeight(30);
        gridPane.setPrefWidth(553);
        ObservableList<ColumnConstraints> columnConstraints = gridPane.getColumnConstraints();
        ColumnConstraints columnConstraint1 = new ColumnConstraints();
        columnConstraint1.setMaxWidth(292);
        columnConstraint1.setPrefWidth(155);

        ColumnConstraints columnConstraint2 = new ColumnConstraints();
        columnConstraint2.setMaxWidth(497);
        columnConstraint2.setPrefWidth(304);

        ColumnConstraints columnConstraint3 = new ColumnConstraints();
        columnConstraint3.setMaxWidth(497);
        columnConstraint3.setPrefWidth(99);

        columnConstraints.add(columnConstraint1);
        columnConstraints.add(columnConstraint2);
        columnConstraints.add(columnConstraint3);
        Label nameLabel = new Label();
        nameLabel.setText(name);
        GridPane.setColumnIndex(nameLabel, 0);
        Label desc = new Label();
        desc.setText(text);
        GridPane.setColumnIndex(desc, 1);
        Button open = new Button();
        open.setText("Open");
        open.setOnAction(eventHandler);
        GridPane.setColumnIndex(open, 2);
        gridPane.getChildren().addAll(nameLabel, desc, open);
        return gridPane;
    }
}
