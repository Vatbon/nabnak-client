package v.nevolin.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import v.nevolin.model.Model;
import v.nevolin.model.Settings;
import v.nevolin.view.WindowManager;

public class SettingsController {

    @FXML
    public TextField hostField;

    public void setParameters(Settings settings) {
        hostField.setText(settings.getHost());
    }

    public void submitClicked(ActionEvent actionEvent) {
        Settings settings = new Settings(false);
        settings.setHost(hostField.getText());
        Model.setSettings(settings);
        WindowManager.closeSettings();
    }
}
