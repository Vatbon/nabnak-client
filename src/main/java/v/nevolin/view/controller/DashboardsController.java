package v.nevolin.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Paint;
import lombok.SneakyThrows;
import v.nevolin.entity.client.DashboardEntity;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class DashboardsController {

    @FXML
    public Button joinButton;
    @FXML
    public TextField hashField;
    @FXML
    public Button createDashboardButton;
    @FXML
    public TextField dashboardNameField;
    @FXML
    public Label tooLongLabel;
    @FXML
    public Button ftsButton;
    @FXML
    private TilePane dashboardsPane;
    @FXML
    private HBox exampleHBox;
    @FXML
    private Label roleLabel;
    @FXML
    private Label hashLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private Label notFoundLabel;

    private static final List<Node> hBoxes = new ArrayList<>();


    private void setDashboardRoleList(List<DashboardEntity> dashboardRoleList) {
        hBoxes.clear();
        for (DashboardEntity role : dashboardRoleList) {
            HBox hBox = new HBox();
            hBox.setLayoutX(exampleHBox.getLayoutX());
            hBox.setLayoutY(exampleHBox.getLayoutY());
            hBox.setPrefWidth(exampleHBox.getPrefWidth());
            hBox.setPrefHeight(exampleHBox.getPrefHeight());
            Label roleLabel1 = new Label();
            roleLabel1.setText(role.getRole().getName());
            roleLabel1.setPrefWidth(roleLabel.getPrefWidth());
            roleLabel1.setPrefHeight(roleLabel.getPrefHeight());
            Label hashLabel1 = new Label();
            String hash = role.getDashboard().getHash();
            hashLabel1.setText(hash.substring(0, Math.min(hash.length(), 10)));
            hashLabel1.setPrefWidth(hashLabel.getPrefWidth());
            hashLabel1.setPrefHeight(hashLabel.getPrefHeight());
            Label nameLabel1 = new Label();
            nameLabel1.setText(role.getDashboard().getName());
            nameLabel1.setPrefWidth(nameLabel.getPrefWidth());
            nameLabel1.setPrefHeight(nameLabel.getPrefHeight());
            Separator separator1 = new Separator();
            separator1.setOrientation(Orientation.VERTICAL);
            Separator separator2 = new Separator();
            separator2.setOrientation(Orientation.VERTICAL);
            Button button = new Button();
            button.setOnAction(event -> WindowManager.openDashboard(role.getDashboard().getHash()));
            button.setText("Enter");
            hBox.getChildren().addAll(List.of(roleLabel1, separator1, hashLabel1, separator2, nameLabel1, button));
            hBoxes.add(hBox);
        }
        dashboardsPane.getChildren().clear();
        dashboardsPane.getChildren().addAll(hBoxes);
    }

    @FXML
    public void onJoinClicked() {
        String hash = hashField.getText();
        int res = Model.joinDashboard(hash);
        if (res != 0) {
            notFoundLabel.setVisible(true);
            hashField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            return;
        }
        notFoundLabel.setVisible(false);
        hashField.setStyle("");
        setDashboardRoleList(WindowManager.getDashboards());
    }

    @SneakyThrows
    public void init() {
        setDashboardRoleList(WindowManager.getDashboards());
    }

    public void createClicked(ActionEvent actionEvent) {
        String dashboardName = dashboardNameField.getText();
        if (dashboardName.length() > 256) {
            tooLongLabel.setText("Dashboard name is too long");
            tooLongLabel.setTextFill(Paint.valueOf("RED"));
            tooLongLabel.setVisible(true);
            return;
        }
        tooLongLabel.setVisible(false);
        Model.createDashboard(dashboardName);
        dashboardNameField.setText("");
    }

    public void openFts(ActionEvent actionEvent) {
        WindowManager.openFts();
    }
}
