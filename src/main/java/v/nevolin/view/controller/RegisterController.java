package v.nevolin.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import v.nevolin.entity.db.User;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

public class RegisterController {
    @FXML
    public TextField loginField;
    @FXML
    public TextField emailField;
    @FXML
    public PasswordField passwordField;
    @FXML
    public PasswordField passwordCheckField;
    @FXML
    public Label wrongPassword;

    @FXML
    public void registerClicked(ActionEvent actionEvent) {
        String userName = loginField.getText();
        String email = emailField.getText();
        String password = passwordField.getText();
        String passwordCheck = passwordCheckField.getText();
        passwordCheckField.setStyle("");
        loginField.setStyle("");
        emailField.setStyle("");
        passwordField.setStyle("");
        if (email.length() > 256) {
            wrongPassword.setText("Email is too long");
            wrongPassword.setVisible(true);
            emailField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            return;
        } else if (userName.length() > 64) {
            wrongPassword.setText("Username is too long");
            wrongPassword.setVisible(true);
            loginField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            return;
        } else if (password.length() > 64) {
            wrongPassword.setText("Password is too long");
            wrongPassword.setVisible(true);
            passwordField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            return;
        } else if (!password.equals(passwordCheck)) {
            wrongPassword.setText("Please make sure your passwords match");
            wrongPassword.setVisible(true);
            passwordCheckField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            return;
        }
        User user = new User(0L, userName, email, password);
        int res = Model.register(user);
        if (res != 0) {
            wrongPassword.setText("That login is taken. Please try another");
            wrongPassword.setVisible(true);
            loginField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            return;
        }
        WindowManager.openLogin();
    }

    public void signInClicked(ActionEvent actionEvent) {
        WindowManager.openLogin();
    }
}
