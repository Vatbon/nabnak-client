package v.nevolin.view.controller;

import javafx.util.Pair;

import java.util.List;
import java.util.Random;

public class ColorGenerator {
    private static final List<Pair<String, String>> colors = List.of(
            new Pair<>("#9f46b0", "#e2c7e7"),
            new Pair<>("#00c1e7", "#b2ecf7"),
            new Pair<>("#a9c23f", "#e5ecc5"),
            new Pair<>("#f1c775", "#faeed5")
    );

    private static final List<String> singleColors = List.of(
            "#edb546",
            "#d2ed46",
            "#75f1c7",
            "#8eb0f3"
    );

    private ColorGenerator() {
    }

    public static String getRandomColor(int a) {
        a = ((a * 8645 >> 2) | (a * 86745 << 2)) % singleColors.size();
        return singleColors.get(a);
    }

    public static String getRandomColor() {
        return singleColors.get(new Random().nextInt(singleColors.size()));
    }

    public static Pair<String, String> getRandomPair(int a) {
        a = ((a * 5468 >> 2) | (a * 86745 << 2)) % colors.size();
        return colors.get(a);
    }

    public static Pair<String, String> getRandomPair() {
        return colors.get(new Random().nextInt(colors.size()));
    }
}
