package v.nevolin.view.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import v.nevolin.entity.db.Comment;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

import java.util.Comparator;
import java.util.List;

public class CommentsController {
    @FXML
    public TextArea commentTextFiled;
    @FXML
    public Button commentButton;
    @FXML
    public FlowPane flowPane;
    @FXML
    public Label tooLongLabel;
    private List<Comment> comments;

    public void setComments(List<Comment> commentList) {
        comments = commentList;
        comments.sort(Comparator.comparing(Comment::getCreatedTime));
        flowPane.getChildren().clear();
        for (Comment comment : comments) {
            flowPane.getChildren().add(makeCommentPane(comment));
        }
    }

    public void createComment(ActionEvent actionEvent) {
        Comment comment = new Comment();
        comment.setAuthorUserId(Model.getActiveUser().getId());
        if (commentTextFiled.getText().length() > 1000) {
            tooLongLabel.setText("Comment message is too long");
            tooLongLabel.setTextFill(Paint.valueOf("RED"));
            tooLongLabel.setVisible(true);
            return;
        }
        tooLongLabel.setVisible(false);
        comment.setBody(commentTextFiled.getText());
        comment.setTaskId(WindowManager.getActiveDashboardController().getActiveTaskId());
        Comment newComment = Model.createComment(comment);
        comments.add(newComment);
        setComments(comments);
        commentTextFiled.setText("");
    }

    private GridPane makeCommentPane(Comment comment) {
        GridPane gridPane = new GridPane();
        ObservableList<ColumnConstraints> columnConstraints = gridPane.getColumnConstraints();
        ObservableList<RowConstraints> rowConstraints = gridPane.getRowConstraints();
        columnConstraints.clear();
        rowConstraints.clear();

        gridPane.setPrefWidth(395);
        gridPane.setStyle("-fx-border-color: #000000;");

        ColumnConstraints columnConstraints1 = new ColumnConstraints();
        columnConstraints1.setHgrow(Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(10.0);
        columnConstraints.add(columnConstraints1);

        RowConstraints rowConstraints1 = new RowConstraints();
        rowConstraints1.setVgrow(Priority.ALWAYS);
        rowConstraints1.setMinHeight(10.0);
        rowConstraints.add(rowConstraints1);

        RowConstraints rowConstraints2 = new RowConstraints();
        rowConstraints2.setVgrow(Priority.ALWAYS);
        rowConstraints2.setMinHeight(10.0);
        rowConstraints.add(rowConstraints2);

        RowConstraints rowConstraints3 = new RowConstraints();
        rowConstraints3.setVgrow(Priority.ALWAYS);
        rowConstraints3.setMinHeight(10.0);
        rowConstraints.add(rowConstraints3);

        Label userName = new Label();
        userName.setText(Model.getUserById(comment.getAuthorUserId()).getName());
        GridPane.setRowIndex(userName, 0);
        gridPane.getChildren().add(userName);

        Label body = new Label();
        body.setText(comment.getBody());
        GridPane.setRowIndex(body, 1);
        gridPane.getChildren().add(body);


        Label time = new Label();
        time.setText(comment.getCreatedTime());
        time.setOpacity(0.6);
        GridPane.setRowIndex(time, 2);
        gridPane.getChildren().add(time);

        gridPane.setPadding(new Insets(2));
        FlowPane.setMargin(gridPane, new Insets(2, 0, 2, 0));

        return gridPane;
    }
}
