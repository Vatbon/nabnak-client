package v.nevolin.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import v.nevolin.entity.api.UserDto;
import v.nevolin.entity.db.User;
import v.nevolin.model.Model;

public class UserEditController {
    @FXML
    public TextField nameField;
    @FXML
    public TextField emailField;
    @FXML
    public ComboBox<String> rolesBox;
    private UserDto user;

    public void setUser(UserDto userDto) {
        this.user = userDto;
        nameField.setText(userDto.getUser().getName());
        nameField.setEditable(false);
        emailField.setText(userDto.getUser().getEmail());
        emailField.setEditable(false);
        String[] roles =
                {"Guest", "Participant", "Admin"};
        rolesBox.getItems().addAll(roles);
        rolesBox.setValue(Model.getRoleById(userDto.getRoleId()).getName());
    }

    public void changeRole(ActionEvent actionEvent) {
        UserDto userDto = new UserDto();
        User newUser = new User();
        newUser.setName(user.getUser().getName());
        newUser.setEmail(user.getUser().getEmail());
        newUser.setPassword(user.getUser().getPassword());
        newUser.setId(user.getUser().getId());
        String role = rolesBox.getValue();
        if ("Admin".equals(role)) {
            userDto.setRoleId(0L);
        } else if ("Participant".equals(role)) {
            userDto.setRoleId(1L);
        } else if ("Guest".equals(role)) {
            userDto.setRoleId(2L);
        } else {
            return;
        }
        userDto.setUser(newUser);
        Model.changeRole(userDto);
    }
}
