package v.nevolin.view.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.FlowPane;
import v.nevolin.entity.api.TaskDto;
import v.nevolin.entity.db.Tag;
import v.nevolin.entity.db.Task;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class TaskController {
    @FXML
    public TextField nameField;
    @FXML
    public TextField tagField;
    @FXML
    public Button addButton;
    @FXML
    public TextArea descritptionText;
    @FXML
    public Button createButton;
    @FXML
    public FlowPane tagsFlowPane;
    @FXML
    public ChoiceBox<String> choiceBoxTasks;
    @FXML
    public Label tagTooLongLabel;
    @FXML
    public Label tooLongDescriptionLabel;

    private List<Tag> tagList = new ArrayList<>();
    private boolean isEdit = false;
    private TaskDto taskDto;
    List<TaskDto> taskList = Model.getDashboardDto(Model.getActiveDashboard().getHash()).getTaskList();

    public void setTask(TaskDto task) {
        isEdit = true;
        tagList.clear();
        taskDto = task;
        nameField.setText(task.getTask().getName());
        descritptionText.setText(task.getTask().getDescription());
        task.getTagList().forEach(this::addTag);
        tagField.setText("");
        createButton.setText("Edit");
        choiceBoxTasks.setMouseTransparent(true);
        choiceBoxTasks.setOpacity(0.33);
        prepareBox(false);
    }

    private void prepareBox(boolean isNew) {
        ObservableList<String> items = choiceBoxTasks.getItems();
        items.add("");
        if (isNew) {
            for (TaskDto dto : taskList) {
                items.add(dto.getTask().getName());
            }
        } else {
            for (TaskDto dto : taskList) {
                if (dto.getTask().getId().equals(taskDto.getTask().getParentTaskId())) {
                    choiceBoxTasks.setValue(dto.getTask().getName());
                }
            }
        }
    }

    private void addTag(Tag tag) {
        Label tagLabel = makeTagLabel(tag.getName());
        tagsFlowPane.getChildren().add(tagLabel);
    }

    public void setEmpty() {
        tagList.clear();
        nameField.setText("");
        tagField.setText("");
        descritptionText.setText("");
        createButton.setText("Create");
        isEdit = false;
        prepareBox(true);
    }

    public void addTag() {
        tagField.setStyle("");
        tagTooLongLabel.setVisible(false);
        if (tagField.getText().length() > 40) {
            tagTooLongLabel.setText("Tag name is too long");
            tagTooLongLabel.setVisible(true);
            tagField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            return;
        }
        Label tagLabel = makeTagLabel(tagField.getText());
        tagsFlowPane.getChildren().add(tagLabel);
        tagField.setText("");
    }

    private Label makeTagLabel(String text) {
        Label tagLabel = new Label();
        tagLabel.setText(text);
        FlowPane.setMargin(tagLabel, new Insets(5));
        tagLabel.setPadding(new Insets(1, 4, 1, 4));
        Tag tag = new Tag(0L, text);
        String tagStyle = StyleGenerator.getRandomTagStyle(tag);
        tagLabel.setStyle(tagStyle);
        tagList.add(tag);
        tagLabel.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.SECONDARY)) {
                removeTag(tag, tagLabel);
            }
        });
        tagLabel.setOnMouseEntered(event -> tagLabel.setStyle("-fx-background-color: #aab3b5;" +
                " -fx-background-radius: 10;"));
        tagLabel.setOnMouseExited(event -> tagLabel.setStyle(tagStyle));
        return tagLabel;
    }

    private void removeTag(Tag tag, Label tagLabel) {
        tagList.remove(tag);
        tagsFlowPane.getChildren().remove(tagLabel);
    }

    public void makeTask() {
        Task task;
        tooLongDescriptionLabel.setVisible(false);
        descritptionText.setStyle("");
        nameField.setStyle("");
        if (descritptionText.getText().length() > 1000) {
            descritptionText.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            tooLongDescriptionLabel.setText("Description is too long");
            tooLongDescriptionLabel.setVisible(true);
            return;
        } else if (nameField.getText().length() > 256) {
            nameField.setStyle("-fx-text-box-border: red ;" +
                    "-fx-focus-color: red ;");
            tooLongDescriptionLabel.setText("Task name is too long");
            tooLongDescriptionLabel.setVisible(true);
            return;
        }
        if (isEdit) {
            task = taskDto.getTask();
        } else {
            task = new Task();
            task.setStateId(0L);
        }
        Long parentId = getParentId();
        if (parentId != null) {
            task.setParentTaskId(parentId);
        }
        task.setName(nameField.getText());
        task.setDescription(descritptionText.getText());
        if (isEdit) {
            Model.editTask(task, tagList);
        } else {
            Model.makeTask(task, tagList);
        }
        WindowManager.closeTaskWindow();
        WindowManager.updateDashboard();
    }

    private Long getParentId() {
        String parentName = choiceBoxTasks.getValue();
        if (parentName != null) {
            for (TaskDto dto : taskList) {
                if (parentName.equals(dto.getTask().getName())) {
                    return dto.getTask().getId();
                }
            }
        }
        return null;
    }
}
