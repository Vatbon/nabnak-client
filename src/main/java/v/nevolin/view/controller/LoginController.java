package v.nevolin.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

public class LoginController {

    @FXML
    public TextField loginField;
    @FXML
    public PasswordField passwordFiled;
    @FXML
    public Label wrongLabel;
    @FXML
    public Label connectionLabel;
    @FXML
    public ProgressIndicator progressIndicator;
    private boolean isConnected = false;

    public void setConnected(boolean connected) {
        isConnected = connected;
        if (isConnected) {
            connectionLabel.setText("Connected to the server. Feel free to continue :)");
            connectionLabel.setTextFill(Paint.valueOf("black"));
        } else {
            connectionLabel.setText("Unable to connect to the server. Press to try again.");
            connectionLabel.setTextFill(Paint.valueOf("red"));
        }
    }

    @FXML
    public void loginButtonClicked() {
        if (!isConnected) {
            progressIndicator.setVisible(true);
            setConnected(Model.checkConnection());
            progressIndicator.setVisible(false);
            if (isConnected) {
                login();
            }
        } else {
            login();
        }
    }

    private void login() {
        progressIndicator.setVisible(true);
        int result = Model.login(loginField.getText(), passwordFiled.getText());
        if (result == 0) {
            WindowManager.openDashboards();
        } else {
            wrongLabel.setVisible(true);
            wrongLabel.setText("Wrong password or login. Try again");
        }
        progressIndicator.setVisible(false);
    }

    @FXML
    public void registerButtonClicked() {
        if (isConnected) {
            WindowManager.openRegister();
        }
    }

    public void mouseConnectionLabelClicked(MouseEvent mouseEvent) {
        progressIndicator.setVisible(true);
        setConnected(Model.checkConnection());
        progressIndicator.setVisible(false);
    }

    public void mouseConnetionLabelEntered(MouseEvent mouseEvent) {
        if (!isConnected) {
            connectionLabel.setUnderline(true);
        }
    }

    public void mouseConnetionLabelExited(MouseEvent mouseEvent) {
        connectionLabel.setUnderline(false);
    }

    public void openSettings(ActionEvent actionEvent) {
        WindowManager.openSettings();
    }
}
