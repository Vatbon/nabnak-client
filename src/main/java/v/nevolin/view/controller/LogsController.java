package v.nevolin.view.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import v.nevolin.entity.db.Log;
import v.nevolin.model.Model;
import v.nevolin.view.WindowManager;

import java.awt.*;
import java.util.Comparator;
import java.util.List;

public class LogsController {

    @FXML
    public TextArea commentTextFiled;
    @FXML
    public TextField timeTextField;
    @FXML
    public Button commentButton;
    @FXML
    public FlowPane flowPane;
    @FXML
    public Label tooLongLabel;
    private List<Log> logs;

    public void setLogs(List<Log> logList) {
        logs = logList;
        logs.sort(Comparator.comparing(Log::getCreatedTime));
        flowPane.getChildren().clear();
        for (Log log : logs) {
            flowPane.getChildren().add(makeLogPane(log));
        }
    }

    public void createLog(ActionEvent actionEvent) {
        Log log = new Log();
        log.setAuthorUserId(Model.getActiveUser().getId());
        if (commentTextFiled.getText().length() > 1000){
            tooLongLabel.setText("Log comment is too long");
            tooLongLabel.setTextFill(Paint.valueOf("RED"));
            tooLongLabel.setVisible(true);
            return;
        }
        tooLongLabel.setVisible(false);
        log.setComment(commentTextFiled.getText());
        log.setTaskId(WindowManager.getActiveDashboardController().getActiveTaskId());
        log.setTime(Integer.valueOf(timeTextField.getText()));
        Log newLog = Model.createLog(log);
        logs.add(newLog);
        setLogs(logs);
        commentTextFiled.setText("");
        timeTextField.setText("");
    }

    private GridPane makeLogPane(Log log) {
        GridPane gridPane = new GridPane();
        ObservableList<ColumnConstraints> columnConstraints = gridPane.getColumnConstraints();
        ObservableList<RowConstraints> rowConstraints = gridPane.getRowConstraints();
        columnConstraints.clear();
        rowConstraints.clear();

        gridPane.setPrefWidth(395);
        gridPane.setStyle("-fx-border-color: #000000;");

        ColumnConstraints columnConstraints1 = new ColumnConstraints();
        columnConstraints1.setHgrow(Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(10.0);
        columnConstraints.add(columnConstraints1);

        RowConstraints rowConstraints1 = new RowConstraints();
        rowConstraints1.setVgrow(Priority.ALWAYS);
        rowConstraints1.setMinHeight(10.0);
        rowConstraints.add(rowConstraints1);

        RowConstraints rowConstraints2 = new RowConstraints();
        rowConstraints2.setVgrow(Priority.ALWAYS);
        rowConstraints2.setMinHeight(10.0);
        rowConstraints.add(rowConstraints2);

        RowConstraints rowConstraints3 = new RowConstraints();
        rowConstraints3.setVgrow(Priority.ALWAYS);
        rowConstraints3.setMinHeight(10.0);
        rowConstraints.add(rowConstraints3);

        Label userName = new Label();
        userName.setText(Model.getUserById(log.getAuthorUserId()).getName());
        GridPane.setRowIndex(userName, 0);
        gridPane.getChildren().add(userName);

        Label body = new Label();
        body.setText(log.getComment());
        GridPane.setRowIndex(body, 1);
        gridPane.getChildren().add(body);


        Label time = new Label();
        time.setText(log.getCreatedTime());
        time.setOpacity(0.6);
        GridPane.setColumnIndex(time, 0);
        GridPane.setRowIndex(time, 2);
        gridPane.getChildren().add(time);

        Label logTime = new Label();
        logTime.setText(String.valueOf(log.getTime()));
        GridPane.setColumnIndex(logTime, 1);
        GridPane.setRowIndex(logTime, 2);
        GridPane.setHalignment(logTime, HPos.RIGHT);
        gridPane.getChildren().add(logTime);

        gridPane.setPadding(new Insets(2));
        FlowPane.setMargin(gridPane, new Insets(2, 0, 2, 0));

        return gridPane;
    }
}
