package v.nevolin.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ErrorController {
    @FXML
    public Label errorLabel;
    private Stage stage;

    public void setErrorMessage(String error) {
        errorLabel.setText(error);
    }

    public void closeError(ActionEvent actionEvent) {
        if (stage != null) {
            stage.close();
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
