To launch an application run 
```shell script
mvn javafx:compile -f pom.xml
mvn javafx:run -f pom.xml
```
You can change [Nabnak server](https://gitlab.com/vatbon/nabnak) address in [settings.properties](src/main/resources/settings.properties) 
```
defaultHost = addressToTheNabnakServer
```
[Nabnak server](https://gitlab.com/vatbon/nabnak) can be accessed on **nabnak-server.herokuapp.com**
```
defaultHost = nabnak-server.herokuapp.com
```
Also, you can change host in application itself. Follow next steps:
![loginWindow](etc/images/loginWindow.png)\
![setHost](etc/images/settings.png)